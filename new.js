let quotes= [];

function prepareQuotes()
{

quotes.push("Jeśli zaproponują ci miejsce w statku kosmicznym, nie pytaj, co to za miejsce. Po prostu wsiadaj.<span>Sheryl Sandberg</span>");
quotes.push("Rób to, co uważasz za stosowne. I tak zawsze znajdzie się ktoś, kto uważa inaczej…<span>Michelle Obama</span>");
quotes.push("Najtrudniejsze jest zdecydowanie się na działanie. Reszta to już tylko kwestia wytrwałości.<span>Amelia Earhart</span>");
quotes.push("Być może urodziłaś się bez skrzydeł. Ale najważniejsze, żebyś nie przeszkadzała im wyrosnąć.<span>Coco Chanel</span>");
quotes.push("Kiedyś – nie ma takiego dnia tygodnia.<span>Janet Dailey</span>");
quotes.push("Wznoś się po każdym upadku, padniesz trupem albo urosną Ci skrzydła.<span>Maria von Ebner-Eschenbach</span>");
quotes.push("Dawać bez wahania, tracić bez żalu i zdobywać bez chciwości…<span>George Sand</span>");
quotes.push("Nigdy nie marnuj czasu, próbując wytłumaczyć kim jesteś ludziom, którzy postanowili Cię nie zrozumieć.<span>Dream Hampton</span>");
quotes.push("Dawanie otwiera drogę do otrzymywania.<span>Florence Scovel Shinn</span>");
quotes.push("Nauczyłam się, że droga postępu nie jest ani szybka, ani łatwa.<span>Maria Skłodowska-Curie</span>");
quotes.push("W życiu nie chodzi o czekanie aż burza minie. Chodzi o to, by nauczyć się tańczyć w deszczu.<span>Vivian Green</span>");
quotes.push("Jeśli idziesz za swoim marzeniem, to będziesz testowany.<span>Kim Kiyosaki</span>");
quotes.push("Ego mówi: gdy wszystko się ułoży, wówczas poczuję spokój. Duch mówi: znajdź swój spokój i wszystko się ułoży.<span>Marianne Williamson</span>");
quotes.push("Kiedy ktoś przechodzi właśnie przez burzę, twoja cicha obecność jest potężniejsza niż milion pustych słów.<span>Thema Davis</span>");
quotes.push("Twoje poczucie własnej wartości zależy tylko od ciebie. Nie musisz pozwalać nikomu mówić ci, kim masz być.<span>Beyonce</span>");
quotes.push("Można mieć wszystko. Po prostu nie można mieć wszystkiego jednocześnie.<span>Oprah Winfrey</span>");
quotes.push("Zagłuszanie bólu na jakiś czas sprawia, że powraca ze zdwojoną siłą.<span>J.K. Rowling</span>");
quotes.push("Lepiej być nieszczęśliwą samotnie, niż nieszczęśliwą z kimś innym.<span>Marilyn Monroe</span>");
quotes.push("Podnoszę mój głos – nie po to, by krzyczeć, ale po to, by ci bez głosu mogli zostać usłyszani.<span>Malala Yousafzai</span>");
quotes.push("Kiedy czujesz się piękna i silna w środku, to widać na zewnątrz.<span>Angelina Jolie</span>");
quotes.push("To twoje miejsce na świecie i twoje życie; idź i zrób z nim to, co tylko możesz, uczyń z niego to życie, które chciałbyś przeżyć.<span>Mae Jemison</span>");
quotes.push("Nigdy nie jest za późno – za późno by zmienić swoje życie, za późno by być szczęśliwym.<span>Jane Fonda</span>");
quotes.push("Żyje się tylko raz, ale jeśli żyjesz dobrze, ten raz wystarczy.<span>Mae West</span>");
quotes.push("W życiu nie ma rzeczy, których można żałować; są tylko rzeczy, na których można się uczyć.<span>Jennifer Aniston</span>");
quotes.push("Jeśli masz marzenie, musisz uchwycić się go i nigdy nie puszczać.<span>Carol Burnett</span>");
quotes.push("Jeśli ktoś uśmiecha się do ciebie, odpowiedz mu tym samym.<span>Dolly Parton</span>");
quotes.push("Nie boję się sztormów i burz, dzięki nim uczę się, jak żeglować moim statkiem.<span>Louisa May Alcott</span>");
quotes.push("Marzenie jest formą planowania<span>Gloria Steinem</span>");
quotes.push("Ludzie najczęściej rezygnują ze swej siły poprzez myślenie, że nie mają żadnej.<span>Alice Walker</span>");
quotes.push("Jeśli widzisz, że musisz upaść, upadnij przynajmniej w wielkim stylu.<span>Cate Blanchett</span>");
quotes.push("Bez otwartego umysłu nie da się odnieść sukcesu.<span>Martha Stewart</span>");
quotes.push("Nie ograniczaj siebie. Jesteś wszystkim, co masz.<span>Janis Joplin</span>");
quotes.push("Niezależnie od tego, skąd jesteś, twoje marzenia są ważne.<span>Lupita Nyong’o</span>");
quotes.push("Nie można nikogo nakłonić do zmiany. Każdy z nas pilnuje własnej furtki do zmian, która otwiera się tylko od wewnątrz.<span>Marilyn Ferguson</span>");
quotes.push("Wdzięczność otwiera pełnię życia. Sprawia, że to, co mamy, wystarcza. Zamienia opór w akceptację, chaos w porządek, konfuzję w klarowność.<span>Melody Beattie</span>");
quotes.push("Prawda jest taka, że ludzie, których spotykasz na swej drodze, będą traktować cię identycznie, jak ty traktujesz samą siebie.<span>Miranda Kerr</span>");
quotes.push("Zawsze bądź pierwszorzędną wersją siebie zamiast drugorzędną wersją kogoś innego.<span>Judy Garland</span>");
quotes.push("Dla miłości nie jest ważne, aby ludzie do siebie pasowali, ale by byli razem szczęśliwi.<span>Brigitte Bardot</span>");
quotes.push("Jeżeli osoba, z którą rozmawiasz, zdaje się Ciebie nie słuchać, bądź cierpliwy. Być może ma po prostu mały kłaczek w uchu.<span>Kubuś Puchatek</span>");
quotes.push("Sztuka dawania podarunku polega na tym, aby ofiarować coś, czego nie można kupić w żadnym sklepie.<span>Kubuś Puchatek</span>");
quotes.push("Myślenie nie jest łatwe, ale można się do niego przyzwyczaić.<span>Kubuś Puchatek</span>");
quotes.push("Trochę uwagi i trochę myślenia o innych robi różnicę.<span>Kubuś Puchatek</span>");
quotes.push("Czas zacząć życie, jakie sobie wymarzyłeś.<span>Henry James</span>");
quotes.push("O wielkich zmianach decydują małe epizody. Jedno spotkanie, jedna książka, jedna decyzja. Potem wszystko jest już tylko konsekwencją.<span>Jacek Walkiewicz</span>");
quotes.push("Życie jest zbyt krótkie, żeby otwierać niedobre wino i pić je z nudnymi ludźmi.<span>Mads Mikklesen</span>");
quotes.push("Ludzie są na tyle szczęśliwi, na ile sobie pozwolą.<span>Abraham Lincoln</span>");
quotes.push("To człowiek człowiekowi najbardziej potrzebny jest do szczęścia.<span>Paul Thiry Holbach</span>");
quotes.push("Jeśli nie czujesz się godny, by wyrosły Ci skrzydła, nigdy nie oderwiesz się od ziemi.<span>Nick Vujicic</span>");
quotes.push("Motywacja jest tym co pozwala Ci zacząć. Nawyk jest tym co pozwala Ci wytrwać!<span>Stephen Covey</span>");
quotes.push("Nigdy nie rezygnuj z celu tylko dlatego, że osiągnięcie go wymaga czasu. Czas i tak upłynie.<span>H. Jackson Brown</span>");
quotes.push("Sukces jest najgłośniejszym mówcą świata.<span>Bonaparte Napoleon</span>");
quotes.push("Nic tak nie przyczynia się do szybkiego sukcesu jak cudze błędy.<span>Bacon Roger</span>");
quotes.push("Zdolność słuchania jest połową sukcesu.<span>Coolidge Calvin</span>");
quotes.push("Nigdy, nigdy, nigdy, nigdy się nie poddawaj.<span>Churchill Winston</span>");
quotes.push("Trzeba się nauczyć ponosić porażki. Nie można stworzyć nic nowego, jeżeli nie potrafi się akceptować pomyłek.<span>Knight Charles</span>");
quotes.push("Bądźmy wdzięczni idiotom. Gdyby nie oni, reszta nigdy nie osiągnęłaby sukcesu.<span>Mark Twain</span>");
quotes.push("Skoro jesteś - rób swoje. Nie ważne, że są lepsi od ciebie ludzie, coś tam nie wyszło albo niebo wali ci się na głowę.<span>Krzysztof Kotowski</span>");
quotes.push("Codziennie patrz na świat, jakbyś oglądał go po raz pierwszy.<span>Éric-Emmanuel Schmitt</span>");
quotes.push("Ci, którzy są wystarczająco szaleni, by myśleć, że są w stanie zmienić świat, są tymi, którzy go zmieniają.<span>Walter Isaacson</span>");
quotes.push("Przyjaciele są jak ciche anioły, które podnoszą nas, kiedy nasze skrzydła zapominają, jak latać.<span>Antoine de Saint-Exupéry</span>");
quotes.push("Nie rezygnuj z marzeń (...). Nigdy nie wiesz, kiedy okażą się potrzebne.<span>Carlos Ruiz Zafón</span>");
quotes.push("Dobrze widzi się tylko sercem. Najważniejsze jest niewidoczne dla oczu<span>Antoine de Saint-Exupéry</span>");
quotes.push("Spróbuj być tęczą w czyjejś chmurze.<span>Maya Angelou</span>");
quotes.push("Nic, co jest wartościowe, nie jest łatwe.<span>Nicholas Sparks</span>");
quotes.push("Niektóre nieskończoności są większe niż inne nieskończoności.<span>John Green</span>");
quotes.push("Wszystko, czego pragniesz jest po drugiej stronie strachu.<span>George Addair</span>");
quotes.push("Jesteś jedyną osobą na świecie, która może wykorzystać Twój potencjał.<span>Zig Ziglar</span>");
quotes.push("Porażka jest po prostu okazją, by spróbować ponownie, tym razem bardziej przemyślanie.<span>Henry Ford</span>");
quotes.push("Nawet najdłuższa podróż zaczyna się od pierwszego kroku.<span>Laozi</span>");
quotes.push("Nie popełnia błędu tylko ten, kto nic nie robi.<span>Teodor Roosevelt</span>");
quotes.push("Gdy zamykają się jedne drzwi do szczęścia, otwierają się inne; ale często tak długo patrzymy na zamknięte drzwi, że nie dostrzegamy tych, które zostały otwarte.<span>Helen Keller</span>");
quotes.push("Za dwadzieścia lat bardziej będziesz żałował tego, czego nie zrobiłeś, niż tego, co zrbiłeś. Więc odwiąż liny, opuść bezpieczną przystań. Złap w żagle wiatry. Podróżuj, śnij, odkrywaj.<span>Mark Twain</span>");
quotes.push("Punktem wyjścia wszystkich osiągnięć jest pragnienie.<span>Napoleon Hill</span>");
quotes.push("Bez ciągłego wzrostu i postępu, słowa takie jak poprawa, osiągnięcia i sukces nie mają znaczenia.<span>Benjamin Franklin</span>");
quotes.push("Połączenie sił to początek, pozostanie razem to postęp, wspólna praca to sukces.<span>Henry Ford</span>");
quotes.push("Sukces polega na przechodzeniu od porażki do porażki bez utraty entuzjazmu.<span>Winston Churchill</span>");
quotes.push("Wszystkie nasze marzenia mogą stać się rzeczywistością. Jeżeli mamy odwagę je realizować.<span>Walt Disney</span>");
quotes.push("Nie ma nic złego w świętowaniu sukcesu,ale ważniejsze jest wyciągnięcie nauki z porażki.<span>Bill Gates</span>");
quotes.push("Człowiek potrzebuje swoich trudności, ponieważ są niezbędne do tego, by cieszyć się sukcesem.<span>A.P.J. Abdul Kalam</span>");
quotes.push("Każdy, kto nie popełnia błędów, nie stara się wystarczająco.<span>Wess Robert</span>");
quotes.push("Nie popełnia błędu tylko ten, kto nic nie robi.<span>Teodor Roosevelt</span>");
quotes.push("Zadaj sobie pytanie: Czy mogę dać z siebie więcej? Odpowiedź brzmi zwykle: Tak.<span>Paul Tergat</span>");
quotes.push("Przyszłość należy do tych, którzy wierzą w piękno swoich marzeń.<span>Eleanor Roosevelt</span>");
quotes.push("Najważniejszym i największym triumfem człowieka jest zwycięstwo nad samym sobą.<span>Platon</span>");
quotes.push("Twój czas jest ograniczony, więc nie marnuj go na byciem kimś, kim nie jesteś.<span>Steve Jobs</span>");
quotes.push("Jakość ma większe znaczenie niż ilość.<span>Steve Jobs</span>");
quotes.push("Nie czekaj. Czas nigdy nie będzie właściwy. <span>Napoleon Hill</span>");
quotes.push("Pesymista widzi trudności w każdej okazji, optymista widzi okazję w każdej trudności.<span>Winston Churchill</span>");
quotes.push("To od twoich najbardziej niezadowolonych klientów nauczysz się najwięcej.<span>Bill Gates</span>");
quotes.push("Jeśli nie możesz robić wielkich rzeczy, rób rzeczy małe w wielki sposób.<span>Napoleon Hill</span>");
quotes.push("ie wstydź się swoich porażek, wyciągaj z nich wnioski i próbuj od nowa.<span>Richard Branson</span>");
quotes.push("W świecie, który zmienia się szybko podejściem gwarantującym przegraną jest niepodejmowanie ryzyka.<span>Mark Zuckerberg</span>");
quotes.push("Lepiej zużywać się niż rdzewieć.<span>Denis Diderot</span>");
quotes.push("Nie musisz być świetny, by zacząć, ale musisz zacząć, by być świetnym.<span>Zig Ziglar</span>");
quotes.push("Nie bądź jednym z tych, którzy obawiając się niepowodzenia, niczego nie próbują.<span>Thomas Metron</span>");
quotes.push("Do sukcesy nie ma żadnej windy. Trzeba iść po schodach.<span>Brian Tracy</span>");
quotes.push("W konfrontacji strumienia ze skałą, strumień zawsze wygrywa - nie przez swoją siłę, ale przez wytrwałość.<span>Budda</span>");
quotes.push("To, co możesz uczynić nie jest tylko maleńką kroplą w ogromie oceanu, ale jest właśnie tym, co nadaje znaczenie twojemu życiu.<span>Albert Schweitzer</span>");
quotes.push("Bądź tak dobry, by nie mogli Cię dłużej ignorować.<span>Steve Martin</span>");
quotes.push("Ludzie na ogół dają się łatwiej przekonać przez argumenty, na które sami wpadli, niż przez te, które ktoś im narzucił.<span>Blaise Pascal</span>");
quotes.push("Poświęcaj tyle czasu na ulepszanie siebie, byś nie miał go na krytykę innych.<span>Christian D. Larson</span>");
quotes.push("Jedynymi osobami, z którymi powinieneś wyrównać rachunki są ci, którzy Ci pomogli.<span>John E. Southard</span>");

selectRandomQuote();
}

function selectRandomQuote()
{
    let selectedQuoteIndex=Math.floor(Math.random()*quotes.length);
    let selectedQuote=quotes[selectedQuoteIndex];

    document.getElementById("quote").innerHTML=selectedQuote;
}


