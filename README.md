# Motivating Quotes



## General info

This project contains simple page with generate motivating quotes.



## Technologies

* HTML

* CSS

* JavaScript



## Status

Project in progress.
If You have any advice and ideas don't hesitate to contact me.




## Where You can find me

[My blog about dietetics](https://healthyfitplace.blogspot.com)


[My blog on Instagram](https://www.instagram.com/healthyfitplace/)


[My blog on Facebook](https://www.facebook.com/HealthyFitPlaceblog/)


[My LinkedIn profile](linkedin.com/in/patrycja-surmela/)
